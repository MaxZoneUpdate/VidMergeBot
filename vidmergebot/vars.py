from os import getcwd

from prettyconf import Configuration
from prettyconf.loaders import EnvFile, Environment

env_file = f"{getcwd()}/.env"
config = Configuration(loaders=[Environment(), EnvFile(filename=env_file)])


class Vars:
    CACHE_TIME = int(config("CACHE_TIME", default=5))
    DOWN_PATH = f"{getcwd()}/vidmergebot/downloads"
    BOT_TOKEN = "6391789994:AAF1jFLMyAaUXSp0Ts-owc7aESV7fnvmp4k"
    BOT_ID = BOT_TOKEN.split(":")[0]
    APP_ID = 15830858
    API_HASH = "2c015c994c57b312708fecc8a2a0f1a6"
    WORKERS = int(config("WORKERS", default=16))
    STREAMTAPE_API_PASS = None
    STREAMTAPE_API_USERNAME = None
    MESSAGE_DUMP = -1001860694129
    PREFIX_HANDLER = config("PREFIX_HANDLER", default="/ !").split()
    SUPPORT_GROUP = config("SUPPORT_GROUP", default="DivideSupport")
    AUTH_CHANNEL = -1001862133848
    OWNER_ID = 5468192421
    CAPTION = config("CAPTION", default="By @SourcePleaseOfficial")
    VERSION = config("VERSION", default="v1.1 - Stable")
    STREAMTAPE_DEFAULT = None
    BOT_USERNAME = "VD_PROMAX_ROBOT"
    DB_URI = "mongodb+srv://aio:aio@aio.5z4gxok.mongodb.net/?retryWrites=true&w=majority"
    MAX_VIDEOS = int(config("MAX_VIDEOS", default=10))
    JOIN_CHECK = None
    MAX_NON_JOIN_USAGE = int(config("MAX_NON_JOIN_USAGE", default=2))
    MAX_JOIN_USAGE = int(config("MAX_JOIN_USAGE", default=2))
    LIMIT_USER_USAGE = None
